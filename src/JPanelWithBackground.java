import javax.swing.*;
import java.awt.*;

public class JPanelWithBackground extends JPanel{
    private Image backgroundImage;

    public JPanelWithBackground(String fileName) {
        this(new ImageIcon(fileName).getImage());
    }

    public JPanelWithBackground(Image img) {
        this.backgroundImage = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    public void paintComponent(Graphics g) {
        g.drawImage(backgroundImage, 0, 0, null);
    }
}
