import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ProfilePage extends JFrame {

    Color backgroundColor = new Color(207,235,249);
    Color sectionColor = new Color(180,235,249);
    DataBase dataBase = Main.dataBase;

    public ProfilePage() throws HeadlessException {

        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();

        this.setSize(dim.width, dim.height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("FriendsHUB - ProfilePage");

        //Set main panel
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(5, 4));
        mainPanel.setPreferredSize(new Dimension(500, 400));
        mainPanel.setBackground(this.backgroundColor);

        //Set profile panel
        JPanel profilePanel = new JPanel();
        profilePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 70));
        profilePanel.setPreferredSize(new Dimension(500, 400));
        profilePanel.setBackground(this.backgroundColor);

        //Add profile pic to menu panel
        try {
            BufferedImage myPicture = ImageIO.read(new File("D:\\IP\\proiect-ip\\FriendsHUB\\Images\\a.jpg"));
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            profilePanel.add(picLabel);
        } catch (Exception e) {
            System.out.println("[WARNING] Failed to load profile pic.");
        }

        //Add name to menu panel
        String [] name = dataBase.getName("ionela@yahoo.com").toUpperCase().split(" ");
        JLabel labelFirstName = new JLabel(name[0]);
        labelFirstName.setFont(new Font(labelFirstName.getFont().getName(),
                labelFirstName.getFont().getStyle(), 20));
        profilePanel.add(labelFirstName);

        //Intro panel
        JPanel introPanel = new JPanel();
        introPanel.setLayout(new GridLayout(4, 2));
        introPanel.setPreferredSize(new Dimension(100, 400));
        introPanel.setBackground(this.sectionColor);

        //Intro title
        JLabel labelTitle = new JLabel("Intro      ");
        labelTitle.setFont(new Font(labelTitle.getFont().getName(),
                labelTitle.getFont().getStyle(), 20));
        introPanel.add(labelTitle);

        //Info User
        User user = dataBase.getUserInfoProfilePage("ionela@yahoo.com");
        String date = user.getDate();
        String home = user.getHome();
        String job = user.getJob();
        String school = user.getSchool();

        JLabel labelName = new JLabel("Joined      ", JLabel.LEADING);
        labelTitle.setFont(new Font(labelTitle.getFont().getName(),
                labelTitle.getFont().getStyle(), 20));
        introPanel.add(labelName);

        JLabel labelNameValue = new JLabel(date);
        labelNameValue.setFont(new Font(labelFirstName.getFont().getName(),
                labelFirstName.getFont().getStyle(), 20));
        introPanel.add(labelNameValue);

        mainPanel.add(profilePanel);
        mainPanel.add(introPanel);

        this.add(mainPanel);
        this.setVisible(true);
    }
}
