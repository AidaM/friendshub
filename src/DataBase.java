import java.sql.*;
import java.util.*;
import javafx.util.Pair;

public class DataBase {

    public static Connection connection;

    /*
     * Connect to database
     * Should only be called once at the start of the program
     */
    public static void connectToDB() {
        String connectionUrl = "jdbc:jtds:sqlserver://ionelaip.database.windows.net:1433/ionelaip;ssl=off";
        String user = "IonelaTr@ionelaip";
        String pass = "*abcd12C*";
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            connection = DriverManager.getConnection(connectionUrl, user, pass);
            System.out.println("DB connected.");
        } catch (Exception var5) {
            var5.printStackTrace();
        }
    }

    /*
     * Close database
     * Should only be called once at the end of the program
     */
    public static void closeDB() {
        try {
            connection.close();
        } catch (SQLException var2) {
            System.err.println("SQL Close Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("DB closed.");
    }


    /**
     * Register a user in the data base (inserts into Users table)
     * Home, Job and School will be updated with another method
     * @param email
     * @param password
     * @param name
     * @param date
     */
    public static void registerUser(String email, String password, String name, String date) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Users (id, email, password, name, date, home, job, school) values (?,?,?,?,?,?,?,?);");
            statement.setInt(1, generateId());
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, name);
            statement.setString(5, date);
            statement.setString(6, "");
            statement.setString(7, "");
            statement.setString(8, "");
            statement.executeUpdate();
            System.out.println("Inserting DONE.");
        } catch (SQLException var3) {
            System.err.println("[registerUser] SQL Insert Error");
            var3.printStackTrace(System.err);
            System.exit(0);
        }
    }


    /**
     * Check if a user's login credentials are in the database
     * @param email
     * @param password
     * @return true - user exists, false otherwise
     */
    public static boolean checkLoginCredentials(String email, String password) {
        try {
            String sql = "SELECT email, password FROM Users;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                if (rs.getString("email").equals(email) &&
                        rs.getString("password").equals(password)) {
                    System.out.println("Checking done.");
                    return true;
                }
            }
        } catch (SQLException var2) {
            System.err.println("[checkLoginCredentials] SQL Register Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("Checking done.");
        return false;
    }


    /**
     * Returns the id associated with the given email
     * @param email
     * @return id associated with the given email
     */
    public static int getId(String email) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT id FROM Users WHERE email = ?;");
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getInt("id");
        } catch (SQLException var2) {
            System.out.println("There is no user registered with " + email);
        }
        return -1;
    }

    /**
     * @param id
     * @return the email associated with the given id
     */
    public static String getEmail(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT email FROM Users WHERE id = ?"
            );
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getString("email");
        } catch (SQLException var2) {
            System.out.println("There is no user registered with id " + id);
        }
        return "";
    }


    /**
     * Returns the name associated with the given email
     * @param email
     * @return the name associated with the given email
     */
    public static String getName(String email) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT name FROM Users WHERE email = ?;");
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getString("name");
        } catch (SQLException var2) {
            System.out.println("There is no user registered with " + email);
        }
        return "";
    }


    /**
     * Makes two users friends
     * Adds two entries in the Friendship table: if id1 is in id2's friends list,
     * id2 must be in id1's friends list as well
     * @param email1 user 1
     * @param email2 user 2
     */
    public static void makeFriendship(String email1, String email2) {
        int id1 = getId(email1);
        int id2 = getId(email2);
        if (id1 < 0 || id2 < 0) {
            System.out.println("Couldn't make friendship.");
            return;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Friendships;");
            ResultSet rs = statement.executeQuery();
            if (rs.isBeforeFirst()) {
                rs.next();
                if (rs.getInt("id1") == id1 &&
                        rs.getInt("id2") == id2) {
                    System.out.println("Friendship already exists.");
                    return;
                }
            }

            statement = connection.prepareStatement(
                    "INSERT INTO Friendships (id1, id2) values (?,?);");
            statement.setInt(1, id1);
            statement.setInt(2, id2);
            statement.executeUpdate();

            connection.prepareStatement(
                    "INSERT INTO Friendships (id1, id2) values (?,?);");
            statement.setInt(1, id2);
            statement.setInt(2, id1);
            statement.executeUpdate();
        } catch (SQLException var2) {
            System.err.println("[makeFriendship] SQL Inserting Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("Frienship made.");
    }


    /**
     * Deletes two entries from the Frienships table
     * Removes id1 from id2's friends list and id2 from id1's friends list
     * @param email1 user 1
     * @param email2 user 2
     */
    public static void deleteFriendship(String email1, String email2) {
        int id1 = getId(email1);
        int id2 = getId(email2);
        if (id1 < 0 || id2 < 0) {
            System.out.println("Couldn't delete friendship: Id doesn't exist");
            return;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Friendships WHERE id1 = ? and id2 = ?;");
            statement.setInt(1, id1);
            statement.setInt(2, id2);
            statement.executeUpdate();
            statement = connection.prepareStatement(
                    "DELETE FROM Friendships WHERE id2 = ? and id1 = ?;");
            statement.setInt(1, id1);
            statement.setInt(2, id2);
            statement.executeUpdate();
        } catch (SQLException var2) {
            System.err.println("[deleteFriendship] SQL Inserting Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("Frienship deleted.");
    }


    /**
     * @param email
     * @return an arraylist with the user's friends' ids
     */
    public static ArrayList<Integer> getAllFriends(String email) {
        ArrayList<Integer> friends = new ArrayList<Integer>();
        int id = getId(email);
        if (id < 0) {
            System.out.println("Id doesn't exist.");
            return friends;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT id2 FROM Friendships WHERE id1 = ?;");
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                friends.add(rs.getInt("id2"));
            }
        } catch (SQLException var2) {
            System.err.println("[getAllFriends] SQL Select Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        return friends;
    }


    /**
     * Returns a list with the friends of an user List<name, email>
     * @param email
     * @return List<name, email>
     */
    public ArrayList<Pair<String, String>> getFriendsProfilePage(String email) {
        ArrayList<Pair<String, String>> friendsList = new ArrayList<Pair<String, String>>();
        int id = getId(email);
        if (id < 0) {
            System.out.println("Id doesn`t exist.");
            return friendsList;
        }

        try {
            PreparedStatement statement = connection.prepareStatement(
              "SELECT email, name FROM Users WHERE id in (SELECT id2 FROM Friendships WHERE id1 = ?);"
            );
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String email_friend = rs.getString("email");
                String name = rs.getString("name");
                friendsList.add(new Pair<String, String>(name, email_friend));
            }
        } catch (SQLException var2) {
            System.err.println("[getFriendsProfilePage] SQL Select Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }

        return friendsList;
    }


    /**
     * Checks if the email address is already in the database
     * @param email
     * @return true - email exists, false otherwise
     */
    public static boolean checkEmailExists(String email) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT email FROM Users WHERE email = ?;");
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            if (rs.isBeforeFirst()) {
                return true;
            }
        } catch (SQLException var2) {
            System.err.println("[checkLoginCredentials] SQL Register Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
        return false;
    }

    /**
     * prints info about all the users in the database
     * for debug purposes
     */
    public static void printInfoUsers() {
        System.out.println("\nUsers table\n");
        try {
            String sql = "SELECT * FROM Users;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                System.out.println("id: " + rs.getString("id") + "   email: "
                        + rs.getString("email") + "   password: "
                        + rs.getString("password") + "   name: "
                        + rs.getString("name") + "   birthday: "
                        + rs.getString("date") + "   address: "
                        + rs.getString("home") + "   job: "
                        + rs.getString("job") + "   school: "
                        + rs.getString("school"));
            }
        } catch (SQLException var2) {
            System.err.println("[printInfo] SQL Select Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
    }

    /**
     * prints the Friendship table for debug purposes
     */
    public static void printInfoFriendships() {
        System.out.println("\nFriendships table\n");
        try {
            String sql = "SELECT * FROM Friendships;";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                System.out.println("id1: " + rs.getInt("id1") + " id2: " + rs.getInt("id2"));
            }
        } catch (SQLException var2) {
            System.err.println("[printInfo] SQL Select Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }
    }

    /**
     * generate a random ID
     * @return
     */
    public static int generateId() {
        Random rand = new Random();
        return rand.nextInt(1000);
    }

    /**
     * Profile Page
     * @param email
     * @return User object with all the info, except for the password
     */
    public User getUserInfoProfilePage(String email) {
        User user;
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM USERS WHERE email = ?;"
            );
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            rs.next();
            user = new User(rs.getInt("id"), rs.getString("email"),
                    rs.getString("name"), rs.getString("date"),
                    rs.getString("home"), rs.getString("job"),
                    rs.getString("school"));
            return user;
        } catch (SQLException var2) {
            System.err.println("[getUserInfoProfilePage] SQL Select Error");
            var2.printStackTrace(System.err);
            System.exit(0);
        }

        return null;
    }
}
