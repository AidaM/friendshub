import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {

    JButton loginButton, registerButton;
    JTextField email, password, name, date;
    DataBase dataBase;
    JFrame mainFrame;

    public ButtonListener(JButton loginButton, JTextField email, JTextField password, JFrame mainFrame) {
        this.loginButton = loginButton;
        this.email = email;
        this.password = password;
        this.dataBase = Main.dataBase;
        this.mainFrame = mainFrame;
    }

    public ButtonListener(JButton registerButton, JTextField email, JTextField password,
                            JTextField name, JTextField date, JFrame mainFrame) {
        this.registerButton = registerButton;
        this.email = email;
        this.password = password;
        this.name = name;
        this.date = date;
        this.dataBase = Main.dataBase;
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.loginButton) {
            if (!checkFieldsLogin()) {
                JOptionPane.showMessageDialog(null,
                        "All fields must be completed",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else if (dataBase.checkEmailExists(email.getText())) {
                if (!dataBase.checkLoginCredentials(email.getText(),password.getText())) {
                    JOptionPane.showMessageDialog(null,
                            "E-mail address or password is wrong",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    //TODO open new panel
                    NewsfeedPanel newsfeedPanel = new NewsfeedPanel();
                    this.mainFrame.setVisible(false);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "No FriendsHUB user found with this e-mail address",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == this.registerButton) {
            if (!checkFieldsRegister()) {
                JOptionPane.showMessageDialog(null,
                        "All fields must be completed",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else if (dataBase.checkEmailExists(email.getText())) {
                JOptionPane.showMessageDialog(null,
                        "A FriendsHUB user with that e-mail address is already registered",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                System.out.println("Register DONE.");
                dataBase.registerUser(email.getText(),password.getText(),name.getText(), date.getText());
            }
        }
    }

    public boolean checkFieldsLogin() {
        if (this.email.getText().equals("") || this.password.getText().equals(""))
            return false;
        return true;
    }

    public boolean checkFieldsRegister() {
        if (this.name.getText().equals("") || this.email.getText().equals("") ||
                this.password.getText().equals("") || this.date.getText().equals(""))
            return false;
        return true;
    }
}
