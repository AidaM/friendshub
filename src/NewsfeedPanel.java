import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class NewsfeedPanel extends JFrame{

    Color backgroundColor = new Color(207,235,249);
    DataBase dataBase = Main.dataBase;

    public NewsfeedPanel() {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();

        this.setSize(dim.width, dim.height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("FriendsHUB - Newsfeed");

        //Set main panel
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBackground(this.backgroundColor);

        //Search bar
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new FlowLayout(FlowLayout.LEFT, dim.width/14, dim.height/16));
        searchPanel.setPreferredSize(new Dimension(dim.width, 150));
        searchPanel.setBackground(this.backgroundColor);

        JTextField searchBar = new JTextField("", 60);
        searchBar.setFont(new Font(searchBar.getFont().getName(),
                searchBar.getFont().getStyle(), 20));
        JButton searchButton = new JButton("Search");
        searchButton.setFont(new Font(searchButton.getFont().getName(),
                searchButton.getFont().getStyle(), 20));
        searchPanel.add(searchBar);
        searchPanel.add(searchButton);
        mainPanel.add(searchPanel, BorderLayout.NORTH);

        //Menu bar
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new FlowLayout(FlowLayout.LEFT, dim.width/40, dim.height/20));
        menuPanel.setPreferredSize(new Dimension(400, 700));
        menuPanel.setBackground(this.backgroundColor);

        //Add profile pic to menu panel
        try {
            BufferedImage myPicture = ImageIO.read(new File("Images/a.jpg").getAbsoluteFile());
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            menuPanel.add(picLabel);
        } catch (Exception e) {
            System.out.println("[WARNING] Failed to load profile pic.");
        }

        //Add name to menu panel
        String [] name = dataBase.getName("ionela@yahoo.com").toUpperCase().split(" ");
        JLabel labelFirstName = new JLabel(name[0]);
        labelFirstName.setFont(new Font(labelFirstName.getFont().getName(),
                labelFirstName.getFont().getStyle(), 20));
        menuPanel.add(labelFirstName);

        mainPanel.add(menuPanel, BorderLayout.EAST);


        this.add(mainPanel);
        this.setVisible(true);
    }
}
