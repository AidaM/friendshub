import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import sun.misc.JavaLangAccess;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

// You must extend the JFrame class to make a frame

public class Panel extends JFrame{

    Color backgroundColor = new Color(207,235,249);

    public Panel() {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();

        this.setSize(dim.width, dim.height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("FriendsHUB");

        //Set main panel
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, dim.width/5, dim.height/8));

        //Set main panel color
        mainPanel.setBackground(this.backgroundColor);

        //Set icons
        ImageIcon emailIcon = new ImageIcon(new File("Images/email.png").getAbsolutePath());
        Image imageEmail = emailIcon.getImage();
        Image newImageEmail = imageEmail.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
        ImageIcon newEmailIcon = new ImageIcon(newImageEmail);

        ImageIcon passIcon = new ImageIcon(new File("Images/pass.jpg").getAbsolutePath());
        Image imagePass = passIcon.getImage();
        Image newImagePass = imagePass.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
        ImageIcon newPassIcon = new ImageIcon(newImagePass);

        ImageIcon nameIcon = new ImageIcon(new File("Images/name.png").getAbsolutePath());
        Image imageName = nameIcon.getImage();
        Image newImageName = imageName.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
        ImageIcon newNameIcon = new ImageIcon(newImageName);

        ImageIcon bdIcon = new ImageIcon(new File("Images/bd.png").getAbsolutePath());
        Image imageBD = bdIcon.getImage();
        Image newImageBD = imageBD.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
        ImageIcon newBDIcon = new ImageIcon(newImageBD);

        //Add logo
        try {
            JPanel logoPanel = new JPanel();
            logoPanel.setLayout(new BorderLayout());
            logoPanel.setPreferredSize(new Dimension(800, 200));
            logoPanel.setBackground(this.backgroundColor);
            BufferedImage myPicture = ImageIO.read(new File("Images/fhlogo.png").getAbsoluteFile());
            JLabel picLabel = new JLabel(new ImageIcon(myPicture));
            picLabel.setFont(new Font(picLabel.getFont().getName(),
                    picLabel.getFont().getStyle(), 400));
            logoPanel.add(picLabel, BorderLayout.NORTH);
            mainPanel.add(logoPanel, BorderLayout.NORTH);
        } catch (Exception e) {
            System.out.println("[WARNING] Failed to load logo.");
        }

        //Login panel
        JPanel loginPanel = new JPanel();
        loginPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 70));
        loginPanel.setPreferredSize(new Dimension(500, 400));
        loginPanel.setBackground(this.backgroundColor);

        //E-mail
        JTextField emailLogin = new JTextField("", 25);
        emailLogin.setFont(new Font(emailLogin.getFont().getName(),
                emailLogin.getFont().getStyle(), 20));
        JLabel labelEmail = new JLabel("", newEmailIcon, JLabel.LEADING);
        labelEmail.setFont(new Font(labelEmail.getFont().getName(),
                labelEmail.getFont().getStyle(), 20));

        loginPanel.add(labelEmail);
        loginPanel.add(emailLogin);

        //Password
        JTextField passwordLogin = new JTextField("", 25);
        passwordLogin.setFont(new Font(passwordLogin.getFont().getName(),
                passwordLogin.getFont().getStyle(), 20));
        JLabel labelPasswordLogin = new JLabel("", newPassIcon, JLabel.LEADING);
        labelPasswordLogin.setFont(new Font(labelPasswordLogin.getFont().getName(),
                labelPasswordLogin.getFont().getStyle(), 20));
        loginPanel.add(labelPasswordLogin);
        loginPanel.add(passwordLogin);

        //Login button
        JButton loginButton = new JButton("Login");
        loginButton.setFont(new Font(loginButton.getFont().getName(),
                loginButton.getFont().getStyle(), 30));
        ButtonListener buttonListener = new ButtonListener(loginButton, emailLogin, passwordLogin, this);
        loginButton.addActionListener(buttonListener);
        loginPanel.add(loginButton);

        //Register panel
        JPanel registerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 30));
        registerPanel.setPreferredSize(new Dimension(500, 400));
        registerPanel.setBackground(this.backgroundColor);

        //Register text fields
        //Name
        JTextField nameRegister = new JTextField("", 25);
        nameRegister.setFont(new Font(nameRegister.getFont().getName(),
                nameRegister.getFont().getStyle(), 20));
        JLabel labelNameRegister = new JLabel("", newNameIcon, JLabel.LEADING);
        labelNameRegister.setFont(new Font(labelNameRegister.getFont().getName(),
                labelNameRegister.getFont().getStyle(), 20));
        registerPanel.add(labelNameRegister);
        registerPanel.add(nameRegister);

        //Email
        JTextField emailRegister = new JTextField("", 25);
        emailRegister.setFont(new Font(emailRegister.getFont().getName(),
                emailRegister.getFont().getStyle(), 20));
        JLabel labelEmailRegister = new JLabel("", newEmailIcon, JLabel.LEADING);
        labelEmailRegister.setFont(new Font(labelEmail.getFont().getName(),
                labelEmailRegister.getFont().getStyle(), 20));
        registerPanel.add(labelEmailRegister);
        registerPanel.add(emailRegister);

        //Password
        JTextField passwordRegister = new JTextField("", 25);
        passwordRegister.setFont(new Font(passwordRegister.getFont().getName(),
                passwordRegister.getFont().getStyle(), 20));
        JLabel labelPasswordRegister = new JLabel("", newPassIcon, JLabel.LEADING);
        labelPasswordRegister.setFont(new Font(labelPasswordRegister.getFont().getName(),
                labelPasswordRegister.getFont().getStyle(), 20));
        registerPanel.add(labelPasswordRegister);
        registerPanel.add(passwordRegister);

        //Birthdate
        JTextField dateRegister = new JTextField("", 25);
        dateRegister.setFont(new Font(dateRegister.getFont().getName(),
                dateRegister.getFont().getStyle(), 20));
        JLabel labelDateRegister = new JLabel("", newBDIcon, JLabel.LEADING);
        labelDateRegister.setFont(new Font(labelDateRegister.getFont().getName(),
                labelDateRegister.getFont().getStyle(), 20));
        registerPanel.add(labelDateRegister);
        registerPanel.add(dateRegister);


        JButton registerButton = new JButton("Sign up");
        registerButton.setFont(new Font(registerButton.getFont().getName(),
                registerButton.getFont().getStyle(), 30));
        ButtonListener registerButtonListener = new ButtonListener(registerButton, emailRegister,
                passwordRegister, nameRegister, dateRegister, this);
        registerButton.addActionListener(registerButtonListener);
        registerPanel.add(registerButton);


        // How to add the label to the panel and panel to the frame
        mainPanel.add(loginPanel);
        mainPanel.add(registerPanel);

        this.add(mainPanel);
        this.setVisible(true);
    }
}